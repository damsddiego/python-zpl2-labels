
def test():
    import socket
    mysocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    host = "10.10.0.153"
    port = 9100
    try:
        mysocket.connect((host, port))  # connecting to host
        mysocket.send(b"^XA^A0N,50,50^FO50,50^FDSocket Test^FS^XZ")  # using bytes
        mysocket.close()  # closing connection
    except:
        print("Error with the connection")
def change():
    pass
def data():
    from simple_zpl2 import ZPLDocument, Code128_Barcode, EAN13_Barcode
    from PIL import Image
    import io
    from simple_zpl2 import NetworkPrinter
    zdoc = ZPLDocument()

    zdoc.add_comment('Now some text')
    zdoc.add_field_origin(100,60)
    zdoc.add_font('C', zdoc._ORIENTATION_NORMAL, 20)
    zdoc.add_field_data('producto')  # max:32

    zdoc.add_comment('Now some text')
    zdoc.add_field_origin(525,60)
    zdoc.add_font('C', zdoc._ORIENTATION_NORMAL, 20)
    zdoc.add_field_data('producto')  # max:32

    zdoc.add_comment('Now some text')
    zdoc.add_field_origin(900, 60)
    zdoc.add_font('C', zdoc._ORIENTATION_NORMAL, 20)
    zdoc.add_field_data('producto')  # max:32

    zdoc.add_field_origin(150, 120)
    ean_data = '1303099350094'
    bc = EAN13_Barcode(ean_data, 'N', 70, 'Y')
    zdoc.add_barcode(bc)

    zdoc.add_field_origin(525, 120)
    ean_data = '1303099350094'
    bc = EAN13_Barcode(ean_data, 'N', 70, 'Y')
    zdoc.add_barcode(bc)

    zdoc.add_field_origin(900, 120)
    ean_data = '1303099350094'
    bc = EAN13_Barcode(ean_data, 'N', 70, 'Y')
    zdoc.add_barcode(bc)

    zdoc.add_comment('Now some text')
    zdoc.add_field_origin(100, 220)
    zdoc.add_font('C', zdoc._ORIENTATION_NORMAL, 20)
    zdoc.add_field_data('precio')  # max:32

    zdoc.add_comment('Now some text')
    zdoc.add_field_origin(525, 220)
    zdoc.add_font('C', zdoc._ORIENTATION_NORMAL, 20)
    zdoc.add_field_data('precio')  # max:32

    zdoc.add_comment('Now some text')
    zdoc.add_field_origin(900, 220)
    zdoc.add_font('C', zdoc._ORIENTATION_NORMAL, 20)
    zdoc.add_field_data('precio')  # max:32

    png = zdoc.render_png(label_width=2, label_height=1)
    fake_file = io.BytesIO(png)
    img = Image.open(fake_file)
    # Open image with the default image viewer on your system
    img.show()
    prn = NetworkPrinter('10.10.0.153')
    prn.print_zpl(zdoc)


if __name__=='__main__':
    data()