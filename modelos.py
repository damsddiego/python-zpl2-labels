from pydal import Field,DAL
db=DAL("sqlite://db/sistemas_zebra.db")
db.define_table(
    'productos',
    Field('codigo'),
    Field('descrip'),
    Field('precio'),
    migrate=False
)